// https://leetcode.com/problems/maximum-69-number
/**
 * @param {number} num
 * @return {number}
 */
 var maximum69Number  = function(num) {
    let changed = false
    return (num + '').split('').map(x => 
    {
        if (!changed && x == 6) {
            changed = true;
            return 9;
        }

        return x;
    }).join('');
};