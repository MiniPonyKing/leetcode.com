// https://leetcode.com/problems/two-sum/
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
 var twoSum = function(nums, target) {
    let solution = [];
    nums.forEach((n, i) => {
        nums.forEach((k, j) => {
            if (!solution.length && i != j && n + k == target)
                solution = [i, j];
        });
    });
    return solution;
};